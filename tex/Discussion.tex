\chapter{Discussion}
I developed a model for DA diffusion in the striatum that is essentially made up of two parts, a solver for the reaction diffusion equations and a generator for synthetic dopaminergic
axons. The full model can be found on \url{https://bitbucket.org/Narur/dope-amine}, in the hope that it can be used by others too.
In the course of the development of the diffusion model I also designed a DA receptor activation model that incorporates the slow receptor kinetics of the DA receptors, instead of letting them be infinitely fast. 
\newline

\section{Significance of Findings}

There has been a 3D diffusion model of DA in the striatum before \citep{dreyer2010influence,dreyer2013mathematical,dreyer2016functionally} however it used a smaller domain $\approx 20 \mu m$ and did not explicitly 
study the tempo-spatial structure of the DA distribution. It incorporates a model of receptor binding, however it uses the instant kinetics model, which makes it quite different from my model. 

We designed the model to investigate if the DA signal is monolithic in space. 
The model predicts that the DA distribution is highly variably in both space and time, 
differing strongly from the view of commonly used models e.g. for reinforcement learning that essentially implicitly assume that 
the DA concentration is a flat monolithic signal in the striatum (e.g. \citealt{schultz2007multiple}).
I show, with the help of Fourier transformations, that the source of this highly variable DA signal is the constant spiking release from the dopaminergic axons.
Diffusion does not smooth out these DA maps. Furthermore my model predicts that in the case of PD the spatial DA distribution becomes more homogenous since the quantal release events that make the DA maps very variable become rarer, so that 
diffusion has the chance to flatten out the map. The treatment with Levodopa does not recover the variability of the healthy DA maps.
As predicted by \citet{dreyer2014three} my model also shows passive stabilization in the PD case, the average DA concentration does not reduce during the baseline firing indicating that a reduced DA baseline is not the reason for the motor symptoms of PD.
However, the spatio temporal structure of the DA signal changes in the PD case, which could be the starting point for further investigations into how these motor symptoms arise.    

A way to experimentally test these predictions of spatially variable DA maps would be to compare the spectra of the model DA maps, with the spectra of measured DA maps with the recently developed method of \citet{patriarchi2018ultrafast} and \citet{sun2018genetically}.
\newline

When investigating the receptor activation maps, my model differs considerably from the model presented in \citep{dreyer2010influence}. 
This is due to the fact that the receptors in my model have relatively slow kinetics, where in the dreyer model the react essentially instant to changes in DA concentration.
An exciting finding of the model is that the calculated receptor activation maps have a spatial structure that are strongly correlated with anatomical factors like synaptic positions and the inhomogenous uptake map,
which means that the receptor activation maps have a component that is quite stable in time, since the anatomical factors should not change. 
This remains true even if the receptors are simulated with kinetics up to 100 times faster than the measured values. 
This finding is interesting in the light of the identification of spatially compact neuronal clusters, that are stable for days, in the striatum \citep{barbera2016spatially}.
Of course that does not mean that these clusters are necesarrily formed by a mechanism related to spatially varying DA receptor activation. There are models predicting clustering independent of a spatially 
varying DA receptor map \citep{humphries2009dopamine}. Nevertheless this finding presents a new approach for the formation of these clusters, that might even work in tandem with other clustering approaches.  

The model shows that the instantaneous DA concentration does not change qualitatively when incorporating inhomogenous DA uptake. However there are quantitative changes with a mean difference of around 10 \% of the instantaneous DA 
concentration. With the knowledge that the DA receptors, even if they are fairly fast, can and do pick up small changes in the DA concentration I think that inhomogenous uptake should be taken into account 
for simulations looking at the activation of DA receptors. 
\newline

The model demonstrated that there is a spatial structure for both the DA and the DA activation maps. Both don't behave as the monolithic signal, independent of spatial location, that is often used for models of learning and motor control. 
The DA maps themselves are highly variable in space and time, however this variability is smoothed out by the slow kinetics of the dopamine receptors. What these spatial inhomogenities mean for the models of learning and motor control still has to be 
addressed. However the model presented here could be used as a starting point for these questions. A possible next step to address the effects of the spatial structure of the DA signal would be to combine a spatially resolved neural network simulation of MSNs with our model and directly investigate
the effects of the spatiotemporal inhomogenities of DA on the network.   
\newline

Investigating the spatial structure of the DA signal was not the only goal of my model.  
I was also trying to test if there are inconsistencies in the understanding of DA signaling. 
One inconsistency revealed itself when I incorporated the DA receptor activation into the model of DA diffusion.
Specifically when I incorporated receptor kinetics for the DA receptors.
  
I found that when taking into account the kinetics of the DA receptors, which are quite slow, the commonly assumed separation of receptor types for different temporal DA signals, D1 receptors detect fast high amplitude signals while
D2 receptors detect slow small amplitude signals {\citep{dreyer2010influence,surmeier2007d1,grace2007regulation,schultz2007multiple,frank2006mechanistic}}, breaks down.
Hinting that our understanding of the processing of different temporal components of the DA signal is lacking. I showed that even for receptors that have kinetics up to 100 times faster than the measured kinetics
the separation of DA signal-type and receptor type does not hold true. It is true that our findings are based on a limited number of, fairly old, experiments \citep{burt1976properties,sano1979dopamine,maeno1982dopamine,richfield1989anatomical}.
However the instant kinetics, model essentially argues with no experimental backing and has also never been formally stated, instead it is the result of an implicit assumption.
This assumption being that the dissociation constant describes the fractional occupation of the dopamine receptors well.
Some recent experiments, using genetically altered DA receptors, cast doubt if the DA receptors are really as slow as assumed in our model \citep{patriarchi2018ultrafast,sun2018genetically}.
However, the kinetics of these genetically modified receptors are unlikely to be the same as a wildtype receptor, since their dissociation constants differ from the wildtype receptor by a factor of ten or more.
That means that their kinetics are at least by a factor of $\sqrt{10} \approx 3$ different (assuming, that the on rate is faster and the off rate is slower by the same factor).
On top of that the kinetics measured in \citet{sun2018genetically} still fall within the factor 100 range for which our model predicts a similar response for D1 and D2 receptors independent of the timescale of the signal.     
Furthermore, it is unlikely that the DA receptors are much faster than 100 times their measured value, since then the would have reaction rates in excess of the estimate for the theoretical maximum for enzymes that are embedded in a membrane 
\citep{alberty1958application,eigen1963elementary}. 

Another point to consider with respect to this receptor kinetics dilemma is that, according to our diffusion model, the single location DA concentration time-signal is highly variable even in the 
baseline firing case. It is unlikely that the strong spike-like increases in DA concentration, in these time signals contain useful information, since the spiking release at the dopaminergic synapses is a stochastic process. 
That means that the delivery of one spikelike increase, caused by a release event, can not be controlled very precisely by the dopaminergic axon firing the spikes.
This is exasperated by the low probability of release on an arriving action potential ($p=0.06$). In other words, this means that the single-location DA time-signal is very noisy. 
If the DA receptors are very fast, to the point where the react nearly instantaneous, they would track all of this noise, which would need to be removed by some other process later. 
It is unlikely that this noise can be removed by spatial averaging, since the spatially averaged DA concentration timeline is still quite noisy \textbf{Fig.~\ref{fig:DA_not_a_monolith}}.
However, slower receptor kinetics could act as a low-pass filter, removing some of this noise already at the receptor activation stage. 
Therefore I would argue that it is unlikely that the receptors are nearly instantaneous.

Of course the final goal is to strengthen or dismiss this reasoning with a better estimate of these kinetic parameters. 
There seems to be a large body of work on the intracellular cascade started by the DA receptors\citep{lindskog2006transient,nair2015sensing}. 
However, the beginning step, the kinetics of the binding and unbinding of the DA receptors to DA is not well constrained by experiments. 
I hope that my work can motivate new measurements of these kinetics to better constrain the model of DA receptor binding. 
\newline

\section{Outlook}
The developed model is freely available and will hopefully be used by other researchers to investigate DA and its receptors and their spatiotemporal distribution. 
There are still exciting avenues of research to be done with the model. 

Currently the model is still quite simple, and some straightforward extensions could be incorporated e.g. feedback of D2 autoreceptors on spiking release. 

As mentioned above combining the DA diffusion model with neural network models directly could give a better understanding  on how the
spatiotemporal inhomogenities of the DA distribution influence learning and motor control. Such a combined model would also be useful to follow up on the spatially clustered striatal neurons. 
Investigating pharmacological manipulations, or the effect of different spiketrain types on the spatiotemporal structure of DA and its receptors are also exciting options for future research.
which could be combined with a combined DA diffusion neural network model. 



 











