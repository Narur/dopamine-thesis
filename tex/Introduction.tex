\chapter{ Introduction: Dopamine in the Striatum of the Basal Ganglia}

The Basal Ganglia are a set of subcortical nuclei, that includes the striatum, globus pallidus, subthalamic nucleus (STN) and the substantia nigra, so named for its dark color.
Of special interest for this work are the striatum, one of the the principal input structures of the Basal Ganglia, and the subtantia nigra pars compacta, which sends dopaminergic projections towards the striatum. 
The principal neurons of the striatum are medium spiny neurons (MSN) \citep{kemp1971structure}, which are a special form of GABA-ergic i.e. inhibitory neurons. 
The Basal ganglia is involved in the action selection of motor behavior \citep{redgrave1999basal,hikosaka2000role} .
On of the first models of basal ganglia function proposed a separation of pathways starting from the striatum into a direct and an indirect pathway, based on anatomical separation \citep{albin1989functional}.
In this model the direct pathway projects to the Substantia Nigra pars reticulata and the Globus Pallidus interna, while the projections of the indirect pathway end up in the Globus Pallidus externa. 
The direct pathway is named that way since it directly projects to striatal output structures which in turn send inhibitory projections to the thalamus, suppressing movement.
The direct pathway promotes movement, since activation of the direct pathway inhibits the neurons in the output nuclei that suppress movement. 
The opposing, indirect pathway, suppresses movement, by inhibiting the globus pallidus externa which in turn inhibits the STN which excites the striatal output structures and therefore suppresses movement. 
For this reason these pathways are also often called Go and No-Go pathways. 
An interesting feature of this model is that the anatomically distinct neurons of the different pathways have also physiological differences. One of the most interesting differences is that the neurons of the different pathways 
express different dopamine receptors \citep{gerfen1990d1,le1995d1}. These DA receptors are generally classified as D1 and D2 type receptors. In turn the different MSN types are called D1-MSN and D2-MSN.
It should be noted that D1 type receptors are a family containing the D1 and D5 receptors, and D2 type receptors are a family containing D2, D3, D4 receptors. However generally the D1 and D2 receptors themselves are most numerous.
These receptor types also have diametrically opposed effects on the MSNs. Activation of D1 receptors tends to make MSNs more excitable while activation of D2 receptors tends to make MSNs less excitable {\citep{gerfen1996chapter,surmeier2007d1,gerfen2011modulation}}. 
Since they are both DA receptors, that means that in the model different levels of DA lead to a bias either towards the direct, Go, circuit or towards the indirect, No-Go, circuit.
In the model that means that DA plays an important role in action selection by biasing the Basal Ganglia either towards action taking or action suppression. 
The model is also able to successfully explain the bradykinesia of Parkinsons disease (PD). One of the clinical signs of PD is the death of dopaminergic neurons in the Substantia nigra pars compacta \citep{jellinger2002recent}.
During PD the amount of dopaminergic neurons innervating the striatum reduces. In the model it is assumed that this leads to a reduction in DA concentration in the striatum, biasing the system towards the No-Go pathway 
which leads to trouble initiating movement. 
Sadly, the model turned out to be not entirely correct, the organization and function of the Basal Ganglia is more complicated than initially assumed \citep{redgrave2010goal,calabresi2014direct,nelson2014reassessing}. 
However the interesting problem of the separation of MSNs into distinct classes with different DA receptors and their modulation through DA remains. 
In this work I am less interested about the specific makeup of the Basal Ganglia circuits, and more about how the MSNs in the striatum are modulated through DA.


\section{Dopamine}
DA modulates the function of the striatum, however this modulation it is not as straightforward as I implied above. I made it look like the function of DA simply changes the excitability of MSNs, however that was an oversimplification. 
DA has complex effects on the activity of striatal neurons and seems to have a myriad of functions within the Basal ganglia. 
On a physiological level it changes their excitability {\citep{day2008differential}}, depending on the type of DA receptors (D1 or D2) the MSNs posess, 
and can change the strength of synaptic inputs {\citep{reynolds2001cellular,reynolds2002dopamine}}. 
These seemingly simple physiological effects lead to its diverse roles in motor control {\citep{syed2016action}}, action-selection {\citep{redgrave2010goal}}, 
reinforcement learning {\citep{schultz2007multiple}}, motivation \citep{phillips2008top} and addiction {\citep{everitt2005neural}}. 
However what the exact mechanism of DA in all these roles is remains a matter of debate.
\newline

DA usually is being implicated with a motivational function, however the specifics of this relation are complex and sometimes difficult to unravel.
It is also generally referred to as the reward system, but exactly what this means with relation to reinforcement and motivation is hard to pin down {\citep{{salamone2012mysterious}}.
In popular literature it is often stated that DA codes for ``pleasure'' or ``liking'', possibly because of its role in reward processing.
However it has been established that ``liking'' and ``wanting'' are neurobiologically dissociable, meaning they do not have to coincide.
Furthermore experiments indicate that DA does not code for ``liking'', but infers a motivational ``wanting'' component referred to as incentive salience {\citep{berridge1998role,berridge2007debate}}.

Dopaminergic neurons fire in a way that is consistent with them acting as a reward prediction error (RPE) {\citep{schultz1998predictive}}, meaning they fire when an unpredicted reward is presented and show reduced firing when a predicted reward 
is not delivered. This RPE type firing has been shown to be causally linked to cue reward type learning by showing that stimulation of dopaminergic neurons at the time of reward delivery cancels blocking {\citep{{steinberg2013causal}}.   
This learning role of the DA system fits well with its function in modifying synaptic strengths.  
The RPE responses have not just been detected in the firing of dopaminergic neurons, but have also been directly measured in the DA concentration {\citep{hamid2016mesolimbic}}. However they are superimposed
on a ramping DA signal that seems to increase with the approach to a goal, which is believed to be more motivational in nature {\citep{hamid2016mesolimbic}}.  

DA also plays a role in Action selection believed to be facilitated by the different effects of the D1 and D2 receptors on the excitability of MSNs {\citep{redgrave2010goal}}.
This role in action selection was first proposed due to the motor symptoms observed in PD. PD first presents clinically as a loss of dopaminergic neurons in the Substantia nigra pars compacta, 
which is believed to change dopaminergic signaling which in turn leads to the motor-symptoms. However, that role in action selection is more complicated than initially imagined in the simple Go, No-Go pathway model mentioned above. One theory is that DA influences if behaviour is executed  
 in a goal-directed or habitual fashion {\citep{redgrave2010goal}}. The disruption of the DA system during PD leads, in this model, to a an overreliance on behaviour executed in the goal-directed mode. 

On top of that DA also plays an important role in drug addiction since a number of drugs of abuse, e.g. cocaine, directly influence the DA system and therefore bypass the normal adaptive processes related to normal rewards\citep{di2007reward}.  
This change of the DA concentration seems to interfere with the normal motivational and learning function of DA. Addiction seems to consist of two parts: First, context independent e.g. compensatory adaption processes like altered presynaptic release.
Second context dependent e.g. conditioned responses to drug related stimuli {\citep{berke2000addiction}}. The context dependent part of addiction can be seen as a type of learning gone wrong activating a compulsive stimulus response behavior which could indicate 
a mismatch between the goal directed and habitual execution of behaviors. 
\newline 


To complicate matters the differing functions of DA are assumed to happen on differing timescales  \citep{niv2007tonic} as does the striatal DA concentration ([DA]) {\citep{schultz2007multiple}}. 
Fast, abrupt increases in [DA] lasting for $\approx 1-3 s$ result from phasic bursts in DA neurons  {\citep{roitman2004dopamine}}, which signal reward-related information {\citep{schultz2007multiple,grace2007regulation}}. 
Slightly slower [DA] ramps occur when rats approach a goal location {\citep{howe2013prolonged}} or perform a reinforcement learning task {\citep{hamid2016mesolimbic}}. 
Finally, slow tonic spontaneous firing of DA neurons controls the baseline [DA] and may change on a timescale of minutes or longer {\citep{grace2007regulation}}.
However, if these fast and slow changes in [DA] actually represent distinct signalling modes has recently been called into question {\citep{berke2018does}}.
\newline

On top of all this DA is distinct from other neurotransmitters like GABA and Glutamate in that it is volume transmitted \citep{cragg2004dancing,rice2008dopamine}.
It does not just get released at a presynaptic element and acts by activating the postsynaptic receptors, rather it can leave the synaptic cleft \citep{garris1994efflux} and distribute in space, activating 
receptors not only on the postsynaptic element but farther away. This is corroborated by finding DA receptors predominantly at extrasynaptic locations \citep{hersch1995electron,sesack1994ultrastructural}.
Furthermore, Dopamine transporters (DAT), that remove DA from the extracellular space (ECS) of the striatum are found in extrasynaptic locations \citep{nirenberg1996dopamine}.
The efflux of glutamate out of its synapses is largely limited by the high density of glutamate transporters close to the synapses \citep{lehre1998number}. 
However the density of DAT is much lower \citep{garris1994efflux} and DAT are removing DA at a lower rate than the glutamate transporters, further strengthening the case for volume transmission of DA.
Volume transmission leads to DA not only having a time-component, but also a spatial component. 
This spatio-temporal distribution of DA could be quite complicated but has not been studied extensively. 
Often it is even implicitly assumed that the DA concentration only has a time-component, and no microscopic spatial structure (e.g. \citealt{schultz2002getting}). 
Presumably since experimental results usually did not allow for a spatial resolution $<10 \mu m$ \citep{kelly1986bevelled}.  
\newline

What should also be kept in mind when thinking about the DA system in the Basal Ganglia, is that the studies performed use very different experimental techniques.
Some studies measure neural activity of dopaminergic neurons directly  {\citep{schultz1998predictive}}, while others measure DA concentrations {\citep{hamid2016mesolimbic,floresco2003afferent}}.
Some techniques measure DA concentration directly  {\citep{floresco2003afferent}} while others measure changes relative to a reference {\citep{hamid2016mesolimbic}}.
In principle all these measurements should be related, however how exactly they fit together is not always straightforward. Some features e.g. the DA ramps, can be seen in DA concentration measurements, while seemingly 
not having a counterpart in the neural activity of dopaminergic neurons. Furthermore, depending on the utilized technique the 
interpretation of seemingly similar measurements can be quite different e.g. a value signal measured with FCSV could masquerade as an RPE type signal {\citep{berke2018does}}.   
\newline

All in all DA has a lot of functions that are related, however the exact nature of this relation is currently still under debate. DA acts on different timescales that may or may not represent distinct signalling modes. It interacts with two different main receptor types D1 and D2 which have opposing effects on the neurons that express them.
Additionally it is volume transmitted which leads to a spatial component of the DA signal the significance of which is largely unclear. On top of that studies of the DA system utilize a lot of different experimental techniques sometimes giving
conflicting results whose connection is unclear. In summary the DA system still poses a lot of open questions on which a good model could shed some light.   

\section{Theory and Modelling}
This thesis is an almost exclusively theoretical work concerned with mathematical and computational modeling.
What is the purpose of such a a work? First, this thesis is not a big data work. It seems like there is an increasing sentiment that more data in combination with machine learning (sometimes also called artificial intelligence) will 
solve all the problems in neuroscience and other fields. However I don't believe this sentiment. Of course machine learning will find its place in the toolkit of any (neuro-)scientist and it is a powerful tool, however  I believe it will not solve as many problems as promised.
Essentially, I think, these new methods are very effective in finding correlations, together with large datasets they will be able to find a lot of correlations.
However as the old adage goes "correlation does not mean causation". Without knowing why phenomena are correlated we cannot reach a full understanding of the underlying system.
One task of theory and modelling is finding the mechanisms of correlations. That means that one goal of theoretical work is not necessarily hypothesis testing, but rather hypothesis generation, which in the eyes of at least one of my colleagues makes it unscientific.
I would like to stress that this is different from finding a correlation in the data. Finding the correlations is akin to the question "Is there a connection?" while finding the mechanism answers the question "Why is there a connection?".
The generated hypothesis can then subsequently be tested, however these tests might not be in the realm of theory anymore. 

Hypothesis generation is not the only goal of theory. Theory and modeling have another purpose: assembly of information.
Taking pieces of knowledge and trying to assemble them into a model which, hopefully, functions as expected. 
However if it doesn't than one of the mechanisms in the model is likely to be wrong. The goal of the modeler is then to figure out which part does not fit. 
This type of theoretical work is more closely related to hypothesis testing, and therefore in the eyes of my colleague "real science".

These two aspects of modeling work cannot always be disentangled, usually building a model aims to do both. It tests if the parts of the model fit together, and if they do can predict 
behavior that has not been measured yet. A functioning model is a powerful tool, since we have full control over all aspects of it, and can quickly and cheaply do e.g. pharmacological modifications
and can predict what their outcome should be. At this point I also believe we can say we have a decent grasp of the system that is being modeled, to paraphrase Donald Knuth: "Science is what we understand well enough to explain to a computer." 



\section{Model goals, hopes and dreams}
The DA system presents a fruitful landscape for extensive modelling since there are still a lot of open questions and unexplored connections between different experimental approaches. 
However, I had to choose a focus for what kind of model I wanted to design. After reviewing the literature I decided to design a model focused on the volume transmission aspect of DA, 
an aspect I believe has been largely neglected in past studies. There have been successful attempts on simulating volume transmission  \citep{dreyer2010influence,dreyer2012mathematical}, however usually the focus of these studies was not investigating the spatial structure of 
the DA concentration in detail but on the averaged DA timelines that are generated. There seems to be the general opinion that the DA concentration in the striatum can be seen as a monolithical value i.e. it can be seen as a bath that has the same value independent on location and only changes 
with respect to time. Investigating if this is true was the first goal for this model. The model was designed to study the spatio-temporal distribution of DA in the striatum, to see if there is a spatial structure, or if the general view that the DA concentration
can be seen as a constant value in space holds. Additionally I aimed to address the question how DA interacts with its receptors. Here again the focus was on the spatial distribution of receptor activation as an extension of the spatial distribution of DA. 
If there is a spatial structure in the DA or DA receptor activation then theories of higher order function, like learning and motor control, should be cognizant of it since the MSNs leading to this higher order function will, through there receptors, see a signal that 
is not only dependent on time but also on there position in the striatum. 

The model simulates the DA diffusion and receptor interaction in the striatum while trying to be as biologically realistic as possible. 
The attempt to make the model as biologically realistic as possible is used here to get the best possible results, but also as a test of consistency between models of higher order function and the known low level details of the DA system.
The model is kept as realistic as possible in places where it is hard to estimate the exact effect of the modeled details, however approximations are used when it is easy to argue that the effects will be small e.g. the incorporation of unspecific uptake.
As a secondary goal the model also aims to create a connection between different experimental approaches, specifically dopaminergic neuron firing and direct DA concentration measurements.

The model presented in this work can be seen, as a sort of bottom up approach for the understanding of DA function.
In this capacity it does not directly address high order function as such, however I believe that an incomplete understanding of these low order processes will seriously hamper the understanding of higher order function.  

\section{Thesis Outline}
In this thesis I aim to present the computational model of DA diffusion in the striatum I developed during my PhD studies. 
\newline

In \textbf{Chapter 2} I will present a short overview about the basic theory of diffusion, and diffusion in porous media which will be the theoretical foundation of 
the computational model. 
\newline

\textbf{Chapter 3} will give a short overview about numerical models that can be used to solve the diffusion equation, with source terms. I will introduce some common pitfalls and explain 
the numerical method I used in the work presented here. Furthermore I will show that the code I developed solves the diffusion equation correctly.  
\newline

\textbf{Chapter 4} introduces the model I developed to generate synthetic axons, which in turn are used to introduce inhomogeneous uptake and realistic synaptic positioning into the diffusion model. 
This chapter also explains the generation of spiketrains for the dopaminergic neurons. 
\newline

In \textbf{Chapter 5} I will introduce the receptor activation model that is based on realistic receptor kinetics that I developed, at first, to provide realistic source and sink-terms for the diffusion model.
However, the implications of DA receptors having kinetics that are not instantaneous are quite broad and will be discussed in this chapter. The work from this chapter has also already been uploaded to biorxiv \url{https://doi.org/10.1101/444984 } .
\newline

\textbf{Chapter 6} Will show the results of the simulations done with the full model. Mainly showing that the DA landscape is very inhomogeneous, and that the anatomy, synaptic positions and uptake distribution, has an important impact on the 
DA receptor activation maps. 
\newline

\textbf{Chapter 7} will provide a short discussion of the findings of this work.














